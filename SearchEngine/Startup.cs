﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rotativa.AspNetCore;
using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.Mapper;
using SearchEngine.BLL.Services;
using SearchEngine.BLL.Stemmer;
using SearchEngine.BLL.Stemmer.Interfaces;
using SearchEngine.DAL.Interfaces;
using SearchEngine.DAL.Repositories;
using SearchEngine.WEB.Interfaces;
using SearchEngine.WEB.Middleware;
using SearchEngine.WEB.Services;

namespace SearchEngine
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnvironment = hostingEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            InjectServices(services);

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Document}/{action=Index}/{id?}");
            });

            RotativaConfiguration.Setup(env);
        }

        private void InjectServices(IServiceCollection services)
        {
            IMapper configuredMapper = ConfigureMapper();
            services.AddSingleton(configuredMapper);

            services.AddScoped<IUnitOfWork, UnitOfWork>(serviceProvider =>
                new UnitOfWork(Configuration["ConnectionStrings:DefaultConnection"])
            );

            services.AddScoped<IServiceFactory, ServiceFactory>(serviceProvider =>
            {
                IUnitOfWork unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();
                IMapper mapper = serviceProvider.GetRequiredService<IMapper>();
                IHostingEnvironment hostingEnvironment = serviceProvider.GetRequiredService<IHostingEnvironment>();
                IConfiguration configuration = serviceProvider.GetRequiredService<IConfiguration>();

                return new ServiceFactory(unitOfWork, mapper, hostingEnvironment, configuration);
            });
            services.AddScoped<IStemmerFactory, StemmerFactory>();

            services.AddTransient<IFileService, FileService>();
            services.AddTransient<ISearchService, SearchService>(serviceProvider =>
            {
                IFileService fileService = serviceProvider.GetRequiredService<IFileService>();

                return new SearchService(fileService);
            });
        }

        private IMapper ConfigureMapper()
        {
            MapperConfiguration mapperConfiguration = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DocumentAndDocumentDTO());
                mc.AddProfile(new TermAndTermDTO());
                mc.AddProfile(new NgramAndNgramDTO());
            });

            return mapperConfiguration.CreateMapper();
        }
    }
}
