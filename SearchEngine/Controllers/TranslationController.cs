﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.StatisticModels.TranslateModels;
using SearchEngine.WEB.Interfaces;

namespace SearchEngine.WEB.Controllers
{
    public class TranslationController : Controller
    {
        private IServiceFactory _serviceFactory;
        private IFileService _fileService;

        public TranslationController(IServiceFactory serviceFactory, IFileService fileService)
        {
            _serviceFactory = serviceFactory;
            _fileService = fileService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            if (!files.Any())
            {
                return BadRequest();
            }

            try
            {
                foreach (IFormFile file in files)
                {
                    await SaveFileAndCreateTranslationsAsync(file);
                }

                _serviceFactory.CommitChanges();
            }
            catch
            {
                _serviceFactory.RollbackChanges();

                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Translate(string content)
        {
            if (string.IsNullOrWhiteSpace(content))
            {
                return BadRequest();
            }

            string clearedContent = _serviceFactory.TranslationService.PrepareContentForTranslation(content);

            ICollection<WordTranslateModel> wordTranslateModels = await _serviceFactory.TranslationService
                .Translate(clearedContent);

            return Ok(new {
                translateResults = wordTranslateModels
            });
        }

        private async Task SaveFileAndCreateTranslationsAsync(IFormFile file)
        {
            string filePath = await _fileService.Save(file);
            string fileContent = await _fileService.GetContent(filePath);

            ICollection<ParallelCorporaSentenceModel> parallelCorporaSentenceModels = _serviceFactory.TranslationService
                .BuildParallelCorporaSentenceModels(fileContent);

            ICollection<WordTranslateModel> translateModels = _serviceFactory.TranslationService
                .GetTranslateModels(parallelCorporaSentenceModels);

            await _serviceFactory.TranslationService.CreateManyAsync(translateModels);
        }
    }
}
