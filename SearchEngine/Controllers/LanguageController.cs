﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rotativa.AspNetCore;
using SearchEngine.BLL.DTO;
using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.StatisticModels;
using SearchEngine.Common;
using SearchEngine.WEB.Interfaces;

namespace SearchEngine.WEB.Controllers
{
    public class LanguageController : Controller
    {
        private IServiceFactory _serviceFactory;
        private IFileService _fileService;

        public LanguageController(IServiceFactory serviceFactory, IFileService fileService)
        {
            _serviceFactory = serviceFactory;
            _fileService = fileService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(List<IFormFile> files, Language language)
        {
            if (!files.Any())
            {
                return BadRequest();
            }

            try
            {
                foreach (IFormFile file in files)
                {
                    await SaveFileAndDefineLanguageAsync(file, language);
                }

                _serviceFactory.CommitChanges();
            }
            catch
            {
                _serviceFactory.RollbackChanges();

                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> DefineLanguage([FromForm]List<IFormFile> files)
        {
            ICollection<LanguageDefinitionModel> languageDefinitionModels = new List<LanguageDefinitionModel>();

            if (!files.Any())
            {
                return BadRequest();
            }

            try
            {
                foreach (IFormFile file in files)
                {
                    Language? language = null;
                    LanguageDefinitionModel languageDefinitionModel = await SaveFileAndDefineLanguageAsync(file, language);

                    languageDefinitionModel.OrderNgrams();

                    languageDefinitionModels.Add(languageDefinitionModel);
                }

                _serviceFactory.CommitChanges();
            }
            catch
            {
                _serviceFactory.RollbackChanges();

                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return new ViewAsPdf(languageDefinitionModels);
        }

        public async Task<IActionResult> CompareNgrams(string specifiedLanguageDocumentPublicId, string testDocumentPublicId)
        {
            LanguageDefinitionModel languageDefinitionModel = new LanguageDefinitionModel();

            DocumentDTO specifiedLanguageDocumentDTO = await _serviceFactory.DocumentService
                .GetByPublicIdWithNgramsAsync(specifiedLanguageDocumentPublicId);

            DocumentDTO testDocumentDTO = await _serviceFactory.DocumentService.GetByPublicIdWithNgramsAsync(testDocumentPublicId);

            DocumentLanguageModel documentLanguageModel = _serviceFactory.NgramService
                .GetDocumentLanguageModel(specifiedLanguageDocumentDTO, testDocumentDTO);

            languageDefinitionModel.UnrecognizedLanguageDocumentDTO = testDocumentDTO;
            languageDefinitionModel.DocumentLanguageModels.Add(documentLanguageModel);

            languageDefinitionModel.OrderNgrams();

            return new ViewAsPdf(languageDefinitionModel);
        }

        private async Task<LanguageDefinitionModel> SaveFileAndDefineLanguageAsync(IFormFile file, Language? language)
        {
            LanguageDefinitionModel languageDefinitionModel = null;

            string filePath = await _fileService.Save(file, ".html");
            string xPathSelector = "(//div[contains(@class,'content')])[1]";
            string fileContent = await _fileService.ParseHtmlContent(filePath, xPathSelector);

            bool removeStopwords = false;
            IEnumerable<string> termOccurrences = _serviceFactory.TermService
                .GetTermsWithOccurrences(fileContent, removeStopwords)
                .Select(t => t.Key);

            IEnumerable<NgramDTO> ngramDTOs = _serviceFactory.NgramService.GetNgrams(termOccurrences);

            DocumentDTO documentDTO = new DocumentDTO
            {
                PublicId = Guid.NewGuid().ToString(),
                Title = file.FileName,
                Location = filePath,
                NgramDTOs = ngramDTOs.ToList()
            };

            if (language == null)
            {
                languageDefinitionModel = await _serviceFactory.NgramService.DefineLanguageWithStatisticsAsync(documentDTO);

                // Use alphabet service only to show the way it works.
                languageDefinitionModel.AlphabetLanguageModel = _serviceFactory.AlphabetService.DefineLanguageWithStatistics(fileContent);

                language = languageDefinitionModel.Language;
            }
            documentDTO.Language = (Language)language;

            int documentId = await _serviceFactory.DocumentService.CreateAsync(documentDTO);

            await _serviceFactory.NgramService.CreateDocumentNgramsAsync(ngramDTOs, documentId);

            return languageDefinitionModel;
        }
    }
}
