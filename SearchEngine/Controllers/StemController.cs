﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rotativa.AspNetCore;
using SearchEngine.BLL.DTO;
using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.StatisticModels;
using SearchEngine.BLL.Stemmer.Interfaces;
using SearchEngine.WEB.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngine.WEB.Controllers
{
    public class StemController : Controller
    {
        private IServiceFactory _serviceFactory;
        private IStemmerFactory _stemmerFactory;
        private IFileService _fileService;

        public StemController(IServiceFactory serviceFactory, IStemmerFactory stemmerFactory, IFileService fileService)
        {
            _serviceFactory = serviceFactory;
            _stemmerFactory = stemmerFactory;
            _fileService = fileService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload([FromForm]List<IFormFile> files)
        {
            ICollection<DocumentStemModel> documentStemModels = new List<DocumentStemModel>();

            if (!files.Any())
            {
                return BadRequest();
            }

            try
            {
                foreach (IFormFile file in files)
                {
                    DocumentStemModel documentStemModel = await SaveFileAndGetStemsAsync(file);

                    documentStemModels.Add(documentStemModel);
                }

                _serviceFactory.CommitChanges();
            }
            catch
            {
                _serviceFactory.RollbackChanges();

                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return new ViewAsPdf(documentStemModels);
        }

        private async Task<DocumentStemModel> SaveFileAndGetStemsAsync(IFormFile file)
        {
            DocumentStemModel documentStemModel = new DocumentStemModel();

            string filePath = await _fileService.Save(file, ".html");
            string xPathSelector = "(//div[contains(@class,'content')])[1]";
            string fileContent = await _fileService.ParseHtmlContent(filePath, xPathSelector);

            bool removeStopwords = false;
            IEnumerable<KeyValuePair<string, int>> termsWithOccurrences = _serviceFactory.TermService
                .GetTermsWithOccurrences(fileContent, removeStopwords);

            IEnumerable<NgramDTO> ngramDTOs = _serviceFactory.NgramService.GetNgrams(termsWithOccurrences.Select(t => t.Key));

            DocumentDTO documentDTO = new DocumentDTO
            {
                PublicId = Guid.NewGuid().ToString(),
                Title = file.FileName,
                Location = filePath,
                NgramDTOs = ngramDTOs.ToList()
            };

            LanguageDefinitionModel languageDefinitionModel = await _serviceFactory.NgramService.DefineLanguageWithStatisticsAsync(documentDTO);
            documentDTO.Language = languageDefinitionModel.Language;

            IStemmer stemmer = _stemmerFactory.GetStemmer(languageDefinitionModel.Language);

            foreach (KeyValuePair<string, int> termWithOccurrences in termsWithOccurrences)
            {
                documentStemModel.StemModels.Add(new StemModel {
                    Term = termWithOccurrences.Key,
                    Stem = stemmer.Stem(termWithOccurrences.Key),
                    NumberOfOccurrence = termWithOccurrences.Value
                });
            }
            documentStemModel.DocumentDTO = documentDTO;

            await _serviceFactory.DocumentService.CreateAsync(documentDTO);

            return documentStemModel;
        }
    }
}