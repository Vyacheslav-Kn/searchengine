﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SearchEngine.BLL.DTO;
using SearchEngine.BLL.Interfaces;
using SearchEngine.WEB.Interfaces;
using SearchEngine.BLL.StatisticModels;

namespace SearchEngine.WEB.Controllers
{
    public class DocumentController : Controller
    {
        private IServiceFactory _serviceFactory;
        private IFileService _fileService;
        private ISearchService _searchService;

        public DocumentController(IServiceFactory serviceFactory, IFileService fileService, ISearchService searchService)
        {
            _serviceFactory = serviceFactory;
            _fileService = fileService;
            _searchService = searchService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            if (!files.Any())
            {
                return BadRequest();
            }

            try
            {
                foreach (IFormFile file in files)
                {
                    await SaveFileWithTermsAsync(file);
                }

                await _serviceFactory.TermService.CalculateWeightsAsync();

                _serviceFactory.CommitChanges();
            }
            catch
            {
                _serviceFactory.RollbackChanges();

                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        public async Task<IActionResult> Search(string query)
        {
            if (String.IsNullOrEmpty(query))
            {
                return BadRequest();
            }

            IEnumerable<KeyValuePair<string, int>> queryWordsWithOccurrences = _serviceFactory.TermService.GetTermsWithOccurrences(query);

            IEnumerable<DocumentDTO> documentDTOs = await _serviceFactory.DocumentService
                .GetDocumentsWithQueryWord(queryWordsWithOccurrences.Select(x => x.Key));

            IEnumerable<SearchResultModel> searchResults = await _searchService.Search(documentDTOs, queryWordsWithOccurrences.Select(x => x.Key));

            SearchMetricModel metricModel = await CreateMetricModelForSearchResults(searchResults);

            _serviceFactory.CommitChanges();

            return Ok(new {
                searchResults = searchResults.OrderByDescending(r => r.DocumentQuerySimilarity),
                metrics = metricModel
            });
        }

        public async Task<IActionResult> Show(string publicId)
        {
            if (String.IsNullOrEmpty(publicId))
            {
                return BadRequest();
            }

            DocumentDTO documentDTO = await _serviceFactory.DocumentService.GetByPublicIdAsync(publicId);
            if(documentDTO == null)
            {
                return NotFound();
            }

            string fileContent = await _fileService.GetContent(documentDTO.Location);

            return Ok(fileContent);
        }

        private async Task SaveFileWithTermsAsync(IFormFile file)
        {
            string filePath = await _fileService.Save(file);
            string fileContent = await _fileService.GetContent(filePath);

            IEnumerable<KeyValuePair<string, int>> termsWithOccurrences = _serviceFactory.TermService.GetTermsWithOccurrences(fileContent);

            DocumentDTO documentDTO = new DocumentDTO
            {
                PublicId = Guid.NewGuid().ToString(),
                Title = file.FileName,
                Location = filePath
            };
            int documentId = await _serviceFactory.DocumentService.CreateAsync(documentDTO);

            await _serviceFactory.TermService.CreateDocumentTermsAsync(termsWithOccurrences, documentId);
        }

        private async Task<SearchMetricModel> CreateMetricModelForSearchResults(IEnumerable<SearchResultModel> searchResults)
        {
            SearchMetricModel metricModel = new SearchMetricModel();
            if (searchResults.Count() > 0)
            {
                int numberOfSearchResults = searchResults.Count();
                metricModel.NumberOfRelevantResults = numberOfSearchResults;

                int numberOfSavedDocuments = await _serviceFactory.DocumentService.Count();
                metricModel.NotIncludedNotRelevantResults = numberOfSavedDocuments - numberOfSearchResults;
            }

            return metricModel;
        }

        protected override void Dispose(bool disposing)
        {
            _serviceFactory.Dispose();
            base.Dispose(disposing);
        }
    }
}
