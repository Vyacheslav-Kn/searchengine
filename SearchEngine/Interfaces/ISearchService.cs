﻿using SearchEngine.BLL.DTO;
using SearchEngine.BLL.StatisticModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.WEB.Interfaces
{
    public interface ISearchService
    {
        Task<IEnumerable<SearchResultModel>> Search(IEnumerable<DocumentDTO> documentDTOs, IEnumerable<string> queryWords);
    }
}
