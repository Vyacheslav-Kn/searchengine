﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.WEB.Interfaces
{
    public interface IFileService
    {
        Task<string> Save(IFormFile file, string fileExtension = ".txt");
        Task<string> GetContent(string filePath);
        Task<string> ParseHtmlContent(string filePath, string xPathSelector);
        Task<string> GetSnippets(string filePath, IEnumerable<string> keywords);
    }
}
