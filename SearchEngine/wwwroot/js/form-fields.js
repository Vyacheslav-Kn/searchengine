﻿export const uploadFormToIndexTerms = {
    files: 'files'
};

export const searchForm = {
    query: 'query'
};

export const indexingMetricsCalculationForm = {
    numberOfRelevantResults: 'numberOfRelevantResults',
    notIncludedNotRelevantResults: 'notIncludedNotRelevantResults',
    numberOfNotRelevantResults: 'numberOfNotRelevantResults',
    notIncludedRelevantResults: 'notIncludedRelevantResults'
};

export const uploadFormToCreateNGrams = {
    files: 'files',
    language: 'language'
};

export const uploadFormToCreateTranslationDictionary = {
    files: 'files'
};

export const translateForm = {
    content: 'content'
};
