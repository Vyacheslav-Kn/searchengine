﻿export const documentsUploadFormToIndexTerms = 'documentsUploadFormToIndexTerms';

export const documentsUploadAnimationElement = 'documentsUploadAnimationElement';
export const uploadResultsContainer = 'uploadResultsContainer';

export const documentsSearchForm = 'documentsSearchForm';
export const searchResultsContainer = 'searchResultsContainer';

export const indexingMetricsCalculationForm = 'indexingMetricsCalculationForm';
export const indexingMetricsContainer = 'indexingMetricsContainer';

export const documentsUploadFormToCreateNGrams = 'documentsUploadFormToCreateNGrams';
export const fileInputToCreateNGrams = 'fileInputToCreateNGrams';
export const languageSelectToCreateNGrams = 'languageSelectToCreateNGrams';
export const documentsUploadFormToDefineLanguage = 'documentsUploadFormToDefineLanguage';

export const documentsUploadFormToCreateTranslationDictionary = 'documentsUploadFormToCreateTranslationDictionary';
export const fileInputToCreateTranslationDictionary = 'fileInputToCreateTranslationDictionary';

export const translateForm = 'translateForm';
export const inputWithContentToTranslate = 'inputWithContentToTranslate';
export const translateResultsContainer = 'translateResultsContainer';
