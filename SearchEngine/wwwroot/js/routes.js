﻿export const urlToUploadDocumentsToIndexTerms = '/Document/Upload';
export const urlToSearchDocuments = '/Document/Search';
export const urlToShowDocument = '/Document/Show?publicId=';

export const urlToUploadDocumentsToCreateNGrams = '/Language/Upload';

export const urlToUploadDocumentsToCreateTranslationDictionary = '/Translation/Upload';
export const urlToTranslateContent = '/Translation/Translate';
