﻿import {createMessageElement, clearHtmlAndHide, getRequestVerificationToken} from './common.js';
import * as ids from '../elements-ids.js';
import * as routes from '../routes.js';
import * as forms from '../form-fields.js';

const documentsUploadFormToIndexTerms = window[ids.documentsUploadFormToIndexTerms];
const documentsUploadAnimationElement = window[ids.documentsUploadAnimationElement];
const uploadResultsContainer = window[ids.uploadResultsContainer];

const documentsSearchForm = window[ids.documentsSearchForm];
const searchResultsContainer = window[ids.searchResultsContainer];

const indexingMetricsCalculationForm = window[ids.indexingMetricsCalculationForm];
const indexingMetricsContainer = window[ids.indexingMetricsContainer];

documentsUploadFormToIndexTerms.onsubmit = (event) => {
    event.preventDefault();
    clearHtmlAndHide(uploadResultsContainer);

    documentsUploadAnimationElement.classList.remove('hidden');

    const formData = new FormData();
    const documents = document.getElementsByName(forms.uploadFormToIndexTerms.files)[0].files;
    for (let i = 0; i < documents.length; i++) {
        formData.append(forms.uploadFormToIndexTerms.files, documents[i]);
    }

    const antiForgeryToken = getRequestVerificationToken();

    fetch(routes.urlToUploadDocumentsToIndexTerms, {
        method: 'post',
        body: formData,
        headers: {
            'RequestVerificationToken': antiForgeryToken
        }
    })
        .then((response) => {
            let message = 'Files were not uploaded! Please, try again.';
            let className = 'error';

            if (response.ok) {
                documentsUploadFormToIndexTerms.reset();

                message = 'Files were uploaded!';
                className = 'success';
            }

            documentsUploadAnimationElement.classList.add('hidden');

            const messageElement = createMessageElement(message, className);
            uploadResultsContainer.appendChild(messageElement);
            uploadResultsContainer.classList.remove('hidden');
    });
};

documentsSearchForm.onsubmit = (event) => {
    event.preventDefault();
    clearSearchResultElements();

    const searchUrl = new URL(`${location.origin}${routes.urlToSearchDocuments}`);
    const searchParams = {
        query: document.getElementsByName(forms.searchForm.query)[0].value
    };

    searchUrl.search = new URLSearchParams(searchParams);

    fetch(searchUrl)
        .then(response => response.ok ? response.json() : {searchResults: []})
        .then(responseJSON => {
            let {searchResults, metrics} = responseJSON;

            fillSearchResultsContainer(searchResults);

            if (searchResults.length > 0) {
                showIndexingMetricsCalculationForm(metrics);
            }
        });
};

function fillSearchResultsContainer(searchResults) {
    const totalResultsElement = document.createElement('p');
    totalResultsElement.innerText = `Results: ${searchResults.length}`;
    searchResultsContainer.appendChild(totalResultsElement);

    searchResults.forEach(function (searchResult) {
        const searchResultContainer = CreateSearchResultContainer(searchResult);

        searchResultsContainer.appendChild(searchResultContainer);
    });

    searchResultsContainer.classList.remove('hidden');
}

function CreateSearchResultContainer(searchResult) {
    const searchResultContainer = document.createElement('div');
    searchResultContainer.classList.add('searchResultContainer');

    const fileQuerySimilaritySpan = document.createElement('span');
    const fileHeader = document.createElement('h4');
    const fileCreationDateSpan = document.createElement('span');
    const snippetsContainer = document.createElement('div');
    const fileLink = document.createElement('a');

    fileQuerySimilaritySpan.innerText = `Measure of file and query similarity: ${searchResult.documentQuerySimilarity}`;
    fileHeader.innerText = `${searchResult.documentDTO.title}`;
    fileCreationDateSpan.innerText = `Creation date: ${getDateFromDateTime(searchResult.documentDTO.creationDate)}`;

    snippetsContainer.innerText = `Found snippets: ${searchResult.snippets}`;

    fileLink.innerText = 'Show';
    fileLink.setAttribute('href', `${location.origin}${routes.urlToShowDocument}${searchResult.documentDTO.publicId}`);
    fileLink.setAttribute('target', '_blank');

    searchResultContainer.appendChild(fileQuerySimilaritySpan);
    searchResultContainer.appendChild(fileHeader);
    searchResultContainer.appendChild(fileCreationDateSpan);
    searchResultContainer.appendChild(snippetsContainer);
    searchResultContainer.appendChild(fileLink);

    return searchResultContainer;
}

function showIndexingMetricsCalculationForm(metrics) {
    document.getElementsByName(forms.indexingMetricsCalculationForm.numberOfRelevantResults)[0].value = metrics.numberOfRelevantResults;
    document.getElementsByName(forms.indexingMetricsCalculationForm.notIncludedNotRelevantResults)[0].value = metrics.notIncludedNotRelevantResults;

    indexingMetricsCalculationForm.classList.remove('hidden');
}

indexingMetricsCalculationForm.onsubmit = (event) => {
    event.preventDefault();

    const numberOfRelevantResults = document.getElementsByName(forms.indexingMetricsCalculationForm.numberOfRelevantResults)[0].value;
    const notIncludedNotRelevantResults = document.getElementsByName(forms.indexingMetricsCalculationForm.notIncludedNotRelevantResults)[0].value;
    const numberOfNotRelevantResults = document.getElementsByName(forms.indexingMetricsCalculationForm.numberOfNotRelevantResults)[0].value;
    const notIncludedRelevantResults = document.getElementsByName(forms.indexingMetricsCalculationForm.notIncludedRelevantResults)[0].value;

    const metrics = calculateMetrics({
        numberOfRelevantResults,
        notIncludedNotRelevantResults,
        numberOfNotRelevantResults,
        notIncludedRelevantResults
    });

    const metricsTable = createMetricsTable(metrics);
    indexingMetricsContainer.appendChild(metricsTable);

    indexingMetricsContainer.classList.remove('hidden');
};

function createMetricsTable(metrics) {
    clearHtmlAndHide(indexingMetricsContainer);

    const table = document.createElement('table');
    table.classList.add('gridtable');
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
    const headRow = document.createElement('tr');
    for (let el in metrics) {
        const th = document.createElement('th');
        th.appendChild(document.createTextNode(el));
        headRow.appendChild(th);
    }
    thead.appendChild(headRow);
    table.appendChild(thead);

    const tr = document.createElement('tr');
    for (let el in metrics) {
        const td = document.createElement('td');
        td.appendChild(document.createTextNode(metrics[el]));
        tr.appendChild(td);
    }
    tbody.appendChild(tr);

    table.appendChild(tbody);
    return table;
}

function calculateMetrics(params) {
    const {
        numberOfRelevantResults,
        notIncludedNotRelevantResults,
        numberOfNotRelevantResults,
        notIncludedRelevantResults
    } = params;

    const recall = numberOfRelevantResults / (numberOfRelevantResults + notIncludedRelevantResults);

    const precision = numberOfRelevantResults / (numberOfRelevantResults + numberOfNotRelevantResults);

    const accuracy = (numberOfRelevantResults + notIncludedNotRelevantResults) / (numberOfRelevantResults + notIncludedNotRelevantResults
        + numberOfNotRelevantResults + notIncludedRelevantResults);

    const error = (numberOfNotRelevantResults + notIncludedRelevantResults) / (numberOfRelevantResults + notIncludedNotRelevantResults
        + numberOfNotRelevantResults + notIncludedRelevantResults);

    let fMeasure;
    if (recall === 0 || precision === 0) {
        fMeasure = 0;
    } else {
        fMeasure = 2 / (1 / precision + 1 / recall);
    }

    return {
        recall,
        precision,
        accuracy,
        error,
        fMeasure
    };
}

function clearSearchResultElements() {
    clearHtmlAndHide(uploadResultsContainer, searchResultsContainer, indexingMetricsContainer);

    indexingMetricsCalculationForm.reset();
    indexingMetricsCalculationForm.classList.add('hidden');
}

const getDateFromDateTime = (creationDate) => creationDate.slice(0, 10);
