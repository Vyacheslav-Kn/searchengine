﻿import {createMessageElement, clearHtmlAndHide, getRequestVerificationToken} from './common.js';
import * as ids from '../elements-ids.js';
import * as routes from '../routes.js';
import * as forms from '../form-fields.js';

const documentsUploadFormToCreateNGrams = window[ids.documentsUploadFormToCreateNGrams];
const fileInputToCreateNGrams = window[ids.fileInputToCreateNGrams];
const languageSelectToCreateNGrams = window[ids.languageSelectToCreateNGrams];

const documentsUploadAnimationElement = window[ids.documentsUploadAnimationElement];
const uploadResultsContainer = window[ids.uploadResultsContainer];

documentsUploadFormToCreateNGrams.onsubmit = (event) => {
    event.preventDefault();
    clearHtmlAndHide(uploadResultsContainer);

    documentsUploadAnimationElement.classList.remove('hidden');

    const formData = new FormData();
    const documents = fileInputToCreateNGrams.files;
    for (let i = 0; i < documents.length; i++) {
        formData.append(forms.uploadFormToCreateNGrams.files, documents[i]);
    }

    const language = languageSelectToCreateNGrams.options[languageSelectToCreateNGrams.selectedIndex].value;
    formData.append(forms.uploadFormToCreateNGrams.language, language);

    const antiForgeryToken = getRequestVerificationToken();

    fetch(routes.urlToUploadDocumentsToCreateNGrams, {
        method: 'post',
        body: formData,
        headers: {
            'RequestVerificationToken': antiForgeryToken
        }
    })
        .then((response) => {
            let message = 'Files were not uploaded! Please, try again.';
            let className = 'error';

            if (response.ok) {
                documentsUploadFormToCreateNGrams.reset();

                message = 'Files were uploaded!';
                className = 'success';
            }

            documentsUploadAnimationElement.classList.add('hidden');

            const messageElement = createMessageElement(message, className);
            uploadResultsContainer.appendChild(messageElement);
            uploadResultsContainer.classList.remove('hidden');
        });
};
