﻿export function createMessageElement(message, className) {
    const messageElement = document.createElement('span');

    messageElement.innerText = message;
    messageElement.classList.add(className);

    return messageElement;
}

export const clearHtmlAndHide = (...elements) => {
    elements.forEach(function (element) {
        element.classList.add('hidden');
        element.innerHTML = '';
    });
};

export const getRequestVerificationToken = () => document.querySelector('input[name="__RequestVerificationToken"').value;
