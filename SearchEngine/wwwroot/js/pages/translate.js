﻿import { createMessageElement, clearHtmlAndHide, getRequestVerificationToken } from './common.js';
import * as ids from '../elements-ids.js';
import * as routes from '../routes.js';
import * as forms from '../form-fields.js';

const documentsUploadFormToCreateTranslationDictionary = window[ids.documentsUploadFormToCreateTranslationDictionary];
const fileInputToCreateTranslationDictionary = window[ids.fileInputToCreateTranslationDictionary];

const documentsUploadAnimationElement = window[ids.documentsUploadAnimationElement];
const inputWithContentToTranslate = window[ids.inputWithContentToTranslate];
const uploadResultsContainer = window[ids.uploadResultsContainer];

const translateForm = window[ids.translateForm];
const translateResultsContainer = window[ids.translateResultsContainer];

documentsUploadFormToCreateTranslationDictionary.onsubmit = (event) => {
    event.preventDefault();
    clearHtmlAndHide(uploadResultsContainer, translateResultsContainer);

    documentsUploadAnimationElement.classList.remove('hidden');

    const formData = new FormData();
    const documents = fileInputToCreateTranslationDictionary.files;
    for (let i = 0; i < documents.length; i++) {
        formData.append(forms.uploadFormToCreateTranslationDictionary.files, documents[i]);
    }

    const antiForgeryToken = getRequestVerificationToken();

    fetch(routes.urlToUploadDocumentsToCreateTranslationDictionary, {
        method: 'post',
        body: formData,
        headers: {
            'RequestVerificationToken': antiForgeryToken
        }
    })
    .then((response) => {
        let message = 'Files were not uploaded! Please, try again.';
        let className = 'error';

        if (response.ok) {
            documentsUploadFormToCreateTranslationDictionary.reset();

            message = 'Files were uploaded!';
            className = 'success';
        }

        documentsUploadAnimationElement.classList.add('hidden');

        const messageElement = createMessageElement(message, className);
        uploadResultsContainer.appendChild(messageElement);
        uploadResultsContainer.classList.remove('hidden');
    });
};

translateForm.onsubmit = (event) => {
    event.preventDefault();
    clearHtmlAndHide(translateResultsContainer);

    const formData = new FormData();
    const contentToTranslate = inputWithContentToTranslate.value;

    formData.append(forms.translateForm.content, contentToTranslate);

    const antiForgeryToken = getRequestVerificationToken();

    fetch(routes.urlToTranslateContent, {
        method: 'post',
        body: formData,
        headers: {
            'RequestVerificationToken': antiForgeryToken
        }
    })
    .then(response => response.ok ? response.json() : {translateResults: []})
    .then((responseJSON) => {
        let message = 'Content was not translated! Please, try again.';
        let className = 'error';

        const { translateResults } = responseJSON;

        if (translateResults.length > 0) {
            message = 'Content was translated!';
            className = 'success';

            const translationInfoContainer = createTranslationInfoContainer(translateResults);
            translateResultsContainer.appendChild(translationInfoContainer);
        }

        const messageElement = createMessageElement(message, className);
        translateResultsContainer.insertBefore(messageElement, translateResultsContainer.firstChild);

        translateResultsContainer.classList.remove('hidden');
    });
};

function createTranslationInfoContainer(translateResults) {
    const translationInfoContainer = document.createElement('div');

    let translatedPhrase = '';
    const translatedPhraseElement = document.createElement('p');

    translateResults.forEach(function (wordTranslateModel) {
        translatedPhrase += `${wordTranslateModel.translationsWithProbability[0].key} `;
    });
    translatedPhraseElement.innerText = `Translate: ${translatedPhrase}`;

    const translationInfoElement = document.createElement('div');
    translationInfoElement.innerText = JSON.stringify(translateResults, null, 4);

    translationInfoContainer.appendChild(translatedPhraseElement);
    translationInfoContainer.appendChild(translationInfoElement);

    return translationInfoContainer;
};
