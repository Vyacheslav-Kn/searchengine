﻿using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SearchEngine.WEB.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine.WEB.Services
{
    public class FileService : IFileService
    {
        private IConfiguration _configuration;
        private IHostingEnvironment _hostingEnvironment;

        public FileService(IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public async Task<string> Save(IFormFile file, string fileExtension = ".txt")
        {
            string fileName = Path.GetRandomFileName();
            string filePath = $"{_hostingEnvironment.WebRootPath}\\{_configuration["DocumentFolderName"]}\\{fileName}{fileExtension}";

            if (file.Length > 0)
            {
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            return filePath;
        }

        public async Task<string> GetContent(string filePath)
        {
            StringBuilder contentBuilder = new StringBuilder();

            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                while (reader.Peek() >= 0)
                {
                    contentBuilder.AppendLine(await reader.ReadLineAsync());
                }
            }

            return contentBuilder.ToString();
        }

        public async Task<string> GetSnippets(string filePath, IEnumerable<string> keywords)
        {
            string fileContent = await GetContent(filePath);

            StringBuilder snippetsBuilder = new StringBuilder();

            char[] sentenceEndings = new char[] {'.', '?', '!'};

            string[] sentences = fileContent.Split(sentenceEndings);

            char[] sentenceWordSeparators = new char[] {'.', '?', '!', ' ', ';', ':', ','};

            // Find sentences that contain at least one keyword.
            foreach (string sentence in sentences)
            {
                bool containKeyword = sentence.Split(sentenceWordSeparators, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .Intersect(keywords)
                    .Count() > 0;

                if (containKeyword)
                {
                    snippetsBuilder.AppendLine(sentence);
                }
            }

            return snippetsBuilder.ToString();
        }

        public async Task<string> ParseHtmlContent(string filePath, string xPathSelector)
        {
            string fileContent = await GetContent(filePath);

            HtmlDocument pageDocument = new HtmlDocument();
            pageDocument.LoadHtml(fileContent);

            string parsedContent = pageDocument.DocumentNode.SelectSingleNode(xPathSelector).InnerText;

            return parsedContent;
        }
    }
}
