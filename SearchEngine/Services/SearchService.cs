﻿using SearchEngine.BLL.DTO;
using SearchEngine.WEB.Interfaces;
using SearchEngine.BLL.StatisticModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngine.WEB.Services
{
    public class SearchService : ISearchService
    {
        private IFileService _fileService;

        public SearchService(IFileService fileService) {
            _fileService = fileService;
        }

        public async Task<IEnumerable<SearchResultModel>> Search(IEnumerable<DocumentDTO> documentDTOs, IEnumerable<string> queryWords)
        {
            ICollection<SearchResultModel> searchResults = new List<SearchResultModel>();

            foreach (DocumentDTO documentDTO in documentDTOs)
            {
                SearchResultModel searchResultModel = await CreateSearchResultModel(documentDTO, queryWords);

                searchResults.Add(searchResultModel);
            }

            return searchResults;
        }

        private async Task<SearchResultModel> CreateSearchResultModel(DocumentDTO documentDTO, IEnumerable<string> queryWords)
        {
            DocumentDTO searchResultDocumentDTO = (DocumentDTO)documentDTO.Clone();

            double termWeightsCoefficientForNormalization = CalculateTermWeightsCoefficientForNormalization(searchResultDocumentDTO.TermDTOs);

            ICollection<double> documentDTOVector = new List<double>();
            ICollection<int> queryVector = new List<int>();

            int queryContainTerm = 1;
            int queryNotContainTerm = 0;
            List<TermDTO> searchResultTermDTOs = searchResultDocumentDTO.TermDTOs.ToList();

            foreach (TermDTO termDTO in searchResultDocumentDTO.TermDTOs)
            {
                double normalizedTermWeight = termDTO.Weight / termWeightsCoefficientForNormalization;
                documentDTOVector.Add(normalizedTermWeight);

                if (queryWords.Any(w => w == termDTO.Value))
                {
                    queryVector.Add(queryContainTerm);
                }
                else
                {
                    searchResultTermDTOs.Remove(termDTO);
                    queryVector.Add(queryNotContainTerm);
                }
            }

            searchResultDocumentDTO.TermDTOs = searchResultTermDTOs;

            double documentQuerySimilarity = CalculateDocumentQuerySimilarity(documentDTOVector, queryVector);

            string snippets = await _fileService.GetSnippets(searchResultDocumentDTO.Location,
                searchResultDocumentDTO.TermDTOs.Select(t => t.Value));

            return new SearchResultModel
            {
                DocumentDTO = searchResultDocumentDTO,
                DocumentQuerySimilarity = documentQuerySimilarity,
                Snippets = snippets
            };
        }

        private double CalculateTermWeightsCoefficientForNormalization(IEnumerable<TermDTO> termDTOs)
        {
            return Math.Sqrt(termDTOs.Sum(x => Math.Pow(x.Weight, 2)));
        }

        private double CalculateDocumentQuerySimilarity(ICollection<double> documentVector, ICollection<int> queryVector)
        {
            double documentQuerySimilarity = 0;

            double scalarVectorMultiplication = CalculateScalarVectorMultiplication(documentVector, queryVector);

            double documentVectorLength = Math.Sqrt(documentVector.Sum(x => Math.Pow(x, 2)));

            double queryVectorLength = Math.Sqrt(queryVector.Sum(x => Math.Pow(x, 2)));

            documentQuerySimilarity = scalarVectorMultiplication / (documentVectorLength * queryVectorLength);

            return documentQuerySimilarity;
        }

        private double CalculateScalarVectorMultiplication(ICollection<double> documentVector, ICollection<int> queryVector)
        {
            double scalarVectorMultiplication = 0;

            for(int index = 0; index < documentVector.Count(); index++)
            {
                scalarVectorMultiplication += documentVector.ElementAt(index) * queryVector.ElementAt(index);
            }

            return scalarVectorMultiplication;
        }
    }
}
