﻿using SearchEngine.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Interfaces
{
    public interface ITranslationRepository
    {
        Task CreateManyAsync(IEnumerable<Translation> translations);
        Task<IEnumerable<Translation>> GetTranslationsAsync(string word);
    }
}
