﻿using SearchEngine.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Interfaces
{
    public interface ITermRepository : IGenericRepository<Term>
    {
        Task<IEnumerable<Term>> GetSavedTermsAsync(IEnumerable<string> terms);
        Task CreateManyAsync(IEnumerable<KeyValuePair<string, int>> termsWithOccurrences, int documentId);
        Task UpdateManyAsync(IEnumerable<KeyValuePair<int, int>> termIdsWithOccurrences, int documentId);
        Task CalculateWeightsAsync();
    }
}
