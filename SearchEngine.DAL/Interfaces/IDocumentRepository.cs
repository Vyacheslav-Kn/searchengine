﻿using SearchEngine.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Interfaces
{
    public interface IDocumentRepository : IGenericRepository<Document>
    {
        Task<Document> GetByPublicIdAsync(string publicId);
        Task<Document> GetByPublicIdWithNgramsAsync(string publicId);
        Task DeleteAsync(string publicId);
        Task<IEnumerable<Document>> GetDocumentsWithQueryWord(IEnumerable<string> queryWords);
        Task<IEnumerable<Document>> GetDocumentsWithNgrams();
        Task<int> Count();
    }
}
