﻿using System;

namespace SearchEngine.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IDocumentRepository DocumentRepository { get; }
        ITermRepository TermRepository { get; }
        INgramRepository NgramRepository { get; }
        ITranslationRepository TranslationRepository { get; }

        void Commit();
        void Rollback();
    }
}
