﻿using SearchEngine.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Interfaces
{
    public interface INgramRepository
    {
        Task CreateManyAsync(IEnumerable<Ngram> ngrams, int documentId);
    }
}
