﻿using System.Threading.Tasks;

namespace SearchEngine.DAL.Interfaces
{
    public interface IGenericRepository<T>
    {
        Task<int> CreateAsync(T item);
        Task UpdateAsync(T item);
    }
}
