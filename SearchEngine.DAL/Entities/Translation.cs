﻿namespace SearchEngine.DAL.Entities
{
    public class Translation
    {
        public int Id { get; set; }
        public string OriginalValue { get; set; }
        public string TranslatedValue { get; set; }
        public double Probability { get; set; }
    }
}
