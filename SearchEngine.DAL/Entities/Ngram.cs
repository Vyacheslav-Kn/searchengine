﻿namespace SearchEngine.DAL.Entities
{
    public class Ngram
    {
        public int Id { get; set; }
        public string Value { get; set; }

        // This fields are used when slicing personal document info from database
        public int NumberOfOccurrences { get; set; }
    }
}
