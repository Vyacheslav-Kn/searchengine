﻿namespace SearchEngine.DAL.Entities
{
    public class DocumentNgram
    {
        public int DocumentId { get; set; }
        public int NgramId { get; set; }
        public int NumberOfOccurrences { get; set; }
    }
}
