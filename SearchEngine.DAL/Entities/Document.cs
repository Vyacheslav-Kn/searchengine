﻿using SearchEngine.Common;
using System;
using System.Collections.Generic;

namespace SearchEngine.DAL.Entities
{
    public class Document
    {
        public int Id { get; set; }
        public string PublicId { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
        public string Location { get; set; }
        public Language Language { get; set; }
        public ICollection<Term> Terms { get; set; }
        public ICollection<Ngram> Ngrams { get; set; }

        public Document()
        {
            Terms = new List<Term>();
            Ngrams = new List<Ngram>();
        }
    }
}
