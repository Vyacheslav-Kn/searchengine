﻿namespace SearchEngine.DAL.Entities
{
    public class DocumentTerm
    {
        public int DocumentId { get; set; }
        public int TermId { get; set; }
        public int NumberOfOccurrences { get; set; }
        public double Weight { get; set; }
    }
}
