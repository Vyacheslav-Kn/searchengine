﻿using System.Collections.Generic;

namespace SearchEngine.DAL.Entities
{
    public class Term
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int NumberOfDocuments { get; set; }
        public double InverseFrequency { get; set; }
        public ICollection<Document> Documents { get; set; }

        // This fields are used when slicing personal document info from database
        public int NumberOfOccurrences { get; set; }
        public double Weight { get; set; }

        public Term()
        {
            Documents = new List<Document>();
        }
    }
}
