﻿using Dapper;
using SearchEngine.DAL.Common;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Repositories
{
    public class TermRepository : BaseRepository, ITermRepository
    {
        public TermRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<int> CreateAsync(Term term)
        {
            string query = $"INSERT INTO {Tables.Terms} VALUES (N'{term.Value}', {term.NumberOfDocuments}, " +
                $"{term.InverseFrequency.ToString(CultureInfo.InvariantCulture)}); " +
                $"SELECT SCOPE_IDENTITY();";

            int termId = await Connection.ExecuteScalarAsync<int>(query, null, Transaction);

            return termId;
        }

        public async Task UpdateAsync(Term term)
        {
            string query = $"UPDATE {Tables.Terms} SET {Terms.NumberOfDocuments}={term.NumberOfDocuments}, " +
                $"{Terms.InverseFrequency}={term.InverseFrequency.ToString(CultureInfo.InvariantCulture)} " +
                $"WHERE {Terms.Id}={term.Id};";

            await Connection.ExecuteAsync(query, null, Transaction);
        }

        public async Task<IEnumerable<Term>> GetSavedTermsAsync(IEnumerable<string> terms)
        {
            string query = $"SELECT * FROM {Tables.Terms} WHERE {Terms.Value} IN @TermsToAdd;";

            IEnumerable<Term> savedTerms = await Connection.QueryAsync<Term>(query, new {TermsToAdd = terms}, Transaction);

            return savedTerms;
        }

        public async Task CreateManyAsync(IEnumerable<KeyValuePair<string, int>> termsWithOccurrences, int documentId)
        {
            StringBuilder createTermsBuilder = new StringBuilder();
            int initialNumberOfDocuments = 1;

            createTermsBuilder.AppendLine($"DECLARE @lastInsertedTermId INT;");

            foreach (KeyValuePair<string, int> termWithOccurrences in termsWithOccurrences)
            {
                createTermsBuilder.AppendLine($"INSERT INTO {Tables.Terms} ({Terms.Value}, {Terms.NumberOfDocuments}) VALUES (N'{termWithOccurrences.Key}', {initialNumberOfDocuments}); " +
                    $"SET @lastInsertedTermId = SCOPE_IDENTITY(); " +
                    $"INSERT INTO {Tables.DocumentTerms} ({DocumentTerms.DocumentId}, {DocumentTerms.TermId}, {DocumentTerms.NumberOfOccurrences}) " +
                    $"VALUES ({documentId}, @lastInsertedTermId, {termWithOccurrences.Value});");
            }

            string query = createTermsBuilder.ToString();

            await Connection.ExecuteAsync(query, null, Transaction);
        }

        public async Task UpdateManyAsync(IEnumerable<KeyValuePair<int, int>> termIdsWithOccurrences, int documentId)
        {
            StringBuilder updateTermsBuilder = new StringBuilder();
            int termPresenceInDocument = 1;

            updateTermsBuilder.AppendLine($"UPDATE {Tables.Terms} SET {Terms.NumberOfDocuments} = {Terms.NumberOfDocuments} + {termPresenceInDocument} " +
                $"WHERE {Terms.Id} IN @TermIds;");

            foreach (KeyValuePair<int, int> termIdWithOccurrences in termIdsWithOccurrences)
            {
                updateTermsBuilder.AppendLine($"INSERT INTO {Tables.DocumentTerms} ({DocumentTerms.DocumentId}, {DocumentTerms.TermId}, {DocumentTerms.NumberOfOccurrences}) " +
                    $"VALUES ({documentId}, {termIdWithOccurrences.Key}, {termIdWithOccurrences.Value});");
            }

            string query = updateTermsBuilder.ToString();

            await Connection.ExecuteAsync(query,
                new {TermIds = termIdsWithOccurrences.Select(t => t.Key)},
                Transaction);
        }

        public async Task CalculateWeightsAsync()
        {
            string query = $"{StoredProcedures.CalculateTermWeightsForDocuments}";

            await Connection.ExecuteAsync(query, null, Transaction, null, CommandType.StoredProcedure);
        }
    }
}
