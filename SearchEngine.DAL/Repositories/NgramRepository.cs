﻿using Dapper;
using SearchEngine.DAL.Common;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Repositories
{
    public class NgramRepository : BaseRepository, INgramRepository
    {
        public NgramRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task CreateManyAsync(IEnumerable<Ngram> ngrams, int documentId)
        {
            StringBuilder createNgramsBuilder = new StringBuilder();

            createNgramsBuilder.AppendLine($"DECLARE @savedNgramId INT;");

            foreach (Ngram ngram in ngrams)
            {
                createNgramsBuilder.AppendLine($"SELECT @savedNgramId = {Ngrams.Id} FROM {Tables.Ngrams} WHERE {Ngrams.Value} = N'{ngram.Value}'; " +
                    $"IF(@savedNgramId IS NULL) " +
                    $"BEGIN " +
                    $"INSERT INTO {Tables.Ngrams} VALUES (N'{ngram.Value}'); " +
                    $"SET @savedNgramId = SCOPE_IDENTITY(); " +
                    $"END " +
                    $"INSERT INTO {Tables.DocumentNgrams} VALUES ({documentId}, @savedNgramId, {ngram.NumberOfOccurrences}); " +
                    $"SET @savedNgramId = NULL;");
            }

            string query = createNgramsBuilder.ToString();

            await Connection.ExecuteAsync(query, null, Transaction);
        }
    }
}
