﻿using System.Data;

namespace SearchEngine.DAL.Repositories
{
    public class BaseRepository
    {
        protected IDbTransaction Transaction { get; set; }
        protected IDbConnection Connection
        {
            get
            {
                return Transaction.Connection;
            }
        }

        public BaseRepository(IDbTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}
