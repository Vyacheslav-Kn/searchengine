﻿using Dapper;
using SearchEngine.DAL.Common;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Repositories
{
    public class TranslationRepository : BaseRepository, ITranslationRepository
    {
        public TranslationRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task CreateManyAsync(IEnumerable<Translation> translations)
        {
            StringBuilder createTermsBuilder = new StringBuilder();

            foreach (Translation translation in translations)
            {
                createTermsBuilder.AppendLine($"INSERT {Tables.Translations} " +
                    $"VALUES (N'{translation.OriginalValue}', N'{translation.TranslatedValue}', " +
                    $"{translation.Probability.ToString(CultureInfo.InvariantCulture)});");
            }

            string query = createTermsBuilder.ToString();

            await Connection.ExecuteAsync(query, null, Transaction);
        }

        public async Task<IEnumerable<Translation>> GetTranslationsAsync(string word)
        {
            string query = $"SELECT * FROM {Tables.Translations} WHERE {Translations.OriginalValue} = @Word;";

            IEnumerable<Translation> translations = await Connection.QueryAsync<Translation>(query, new { Word = word }, Transaction);

            return translations;
        }
    }
}
