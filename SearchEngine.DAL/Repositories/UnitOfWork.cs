﻿using SearchEngine.DAL.Interfaces;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SearchEngine.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IDocumentRepository _documentRepository;
        private ITermRepository _termRepository;
        private INgramRepository _ngramRepository;
        private ITranslationRepository _translationRepository;

        private bool _disposed;

        public UnitOfWork(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IDocumentRepository DocumentRepository
        {
            get
            {
                return _documentRepository = _documentRepository ?? new DocumentRepository(_transaction);
            }
        }

        public ITermRepository TermRepository
        {
            get
            {
                return _termRepository = _termRepository ?? new TermRepository(_transaction);
            }
        }

        public INgramRepository NgramRepository
        {
            get
            {
                return _ngramRepository = _ngramRepository ?? new NgramRepository(_transaction);
            }
        }

        public ITranslationRepository TranslationRepository
        {
            get
            {
                return _translationRepository = _translationRepository ?? new TranslationRepository(_transaction);
            }
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
            }
            finally
            {
                BeginNewTransaction();
            }
        }

        public void Rollback()
        {
            try
            {
                _transaction.Rollback();
            }
            catch { }
            finally
            {
                BeginNewTransaction();
            }
        }

        private void BeginNewTransaction()
        {
            _transaction.Dispose();
            _transaction = _connection.BeginTransaction();
            ResetRepositories();
        }

        private void ResetRepositories()
        {
            _documentRepository = null;
            _termRepository = null;
            _translationRepository = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
            }
            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}
