﻿using Dapper;
using SearchEngine.DAL.Common;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace SearchEngine.DAL.Repositories
{
    public class DocumentRepository : BaseRepository, IDocumentRepository
    {
        public DocumentRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<int> CreateAsync(Document document)
        {
            string query = $"INSERT INTO {Tables.Documents} VALUES ('{document.PublicId}', N'{document.Title}', @CreationDate, '{document.Location}', {(int)document.Language}); " +
                $"SELECT SCOPE_IDENTITY();";

            int documentId = await Connection.ExecuteScalarAsync<int>(query, new {document.CreationDate}, Transaction);

            return documentId;
        }

        public async Task<Document> GetByPublicIdAsync(string publicId)
        {
            string query = $"SELECT * FROM {Tables.Documents} WHERE {Documents.PublicId}='{publicId}';";

            Document document = await Connection.QueryFirstOrDefaultAsync<Document>(query, null, Transaction);

            return document;
        }

        public async Task DeleteAsync(string publicId)
        {
            string query = $"DELETE FROM {Tables.Documents} WHERE {Documents.PublicId}='{publicId}';";

            await Connection.ExecuteAsync(query, null, Transaction);
        }

        public async Task UpdateAsync(Document document)
        {
            string query = $"UPDATE {Tables.Documents} SET {Documents.Location}='{document.Location}';";

            await Connection.ExecuteAsync(query, null, Transaction);
        }

        public async Task<IEnumerable<Document>> GetDocumentsWithQueryWord(IEnumerable<string> queryWords)
        {
            string query = $"SELECT * FROM {Tables.Documents} " +
                $"INNER JOIN {Tables.DocumentTerms} ON {Tables.DocumentTerms}.{DocumentTerms.DocumentId} = {Tables.Documents}.{Documents.Id} " +
                $"INNER JOIN {Tables.Terms} ON {Tables.Terms}.{Terms.Id} = {Tables.DocumentTerms}.{DocumentTerms.TermId} " +
                $"WHERE {Tables.Documents}.{Documents.Id} IN (" +
                    $"SELECT DISTINCT {DocumentTerms.DocumentId} FROM {Tables.DocumentTerms} " +
                    $"WHERE {DocumentTerms.TermId} IN (SELECT {Terms.Id} FROM {Tables.Terms} WHERE {Terms.Value} IN @QueryWords)" +
                $");";

            Dictionary<int, Document> savedDocuments = new Dictionary<int, Document>();

            await Connection.QueryAsync<Document, DocumentTerm, Term, Document>(query, (document, documentTerm, term) => {
                Document savedDocument;

                if (!savedDocuments.TryGetValue(document.Id, out savedDocument))
                {
                    savedDocuments.Add(document.Id, savedDocument = document);
                }

                term.NumberOfOccurrences = documentTerm.NumberOfOccurrences;
                term.Weight = documentTerm.Weight;

                savedDocument.Terms.Add(term);

                return savedDocument;
            },
            new { QueryWords = queryWords },
            Transaction,
            splitOn: $"{DocumentTerms.DocumentId}, {Terms.Id}");

            return savedDocuments.Values;
        }

        /* Get all documents with ngrams to define language of incoming document based on existing ones. */
        public async Task<IEnumerable<Document>> GetDocumentsWithNgrams()
        {
            string query = $"SELECT * FROM {Tables.Documents} " +
                $"INNER JOIN {Tables.DocumentNgrams} ON {Tables.DocumentNgrams}.{DocumentNgrams.DocumentId} = {Tables.Documents}.{Documents.Id} " +
                $"INNER JOIN {Tables.Ngrams} ON {Tables.Ngrams}.{Ngrams.Id} = {Tables.DocumentNgrams}.{DocumentNgrams.NgramId}";

            Dictionary<int, Document> savedDocuments = new Dictionary<int, Document>();

            await Connection.QueryAsync<Document, DocumentNgram, Ngram, Document>(query, (document, documentNgram, ngram) => {
                Document savedDocument;

                if (!savedDocuments.TryGetValue(document.Id, out savedDocument))
                {
                    savedDocuments.Add(document.Id, savedDocument = document);
                }

                ngram.NumberOfOccurrences = documentNgram.NumberOfOccurrences;

                savedDocument.Ngrams.Add(ngram);

                return savedDocument;
            },
            null,
            Transaction,
            splitOn: $"{DocumentNgrams.DocumentId}, {Ngrams.Id}");

            return savedDocuments.Values;
        }

        public async Task<Document> GetByPublicIdWithNgramsAsync(string publicId)
        {
            string query = $"SELECT * FROM {Tables.Documents} " +
                $"INNER JOIN {Tables.DocumentNgrams} ON {Tables.DocumentNgrams}.{DocumentNgrams.DocumentId} = {Tables.Documents}.{Documents.Id} " +
                $"INNER JOIN {Tables.Ngrams} ON {Tables.Ngrams}.{Ngrams.Id} = {Tables.DocumentNgrams}.{DocumentNgrams.NgramId} " +
                $"WHERE {Tables.Documents}.{Documents.PublicId}='{publicId}';";

            Document savedDocument = null;

            await Connection.QueryAsync<Document, DocumentNgram, Ngram, Document>(query, (document, documentNgram, ngram) => {
                if(savedDocument == null)
                {
                    savedDocument = document;
                }

                ngram.NumberOfOccurrences = documentNgram.NumberOfOccurrences;

                savedDocument.Ngrams.Add(ngram);

                return savedDocument;
            },
            null,
            Transaction,
            splitOn: $"{DocumentNgrams.DocumentId}, {Ngrams.Id}");

            return savedDocument;
        }

        public async Task<int> Count()
        {
            string query = $"SELECT COUNT(*) FROM {Tables.Documents};";

            int numberOfSavedDocuments = await Connection.ExecuteScalarAsync<int>(query, null, Transaction);

            return numberOfSavedDocuments;
        }
    }
}
