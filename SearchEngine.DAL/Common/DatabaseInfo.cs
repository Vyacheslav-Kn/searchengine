﻿namespace SearchEngine.DAL.Common
{
    public static class Tables
    {
        public static string Documents { get { return "Documents"; } }
        public static string Terms { get { return "Terms"; } }
        public static string DocumentTerms { get { return "DocumentTerms"; } }
        public static string Ngrams { get { return "Ngrams"; } }
        public static string DocumentNgrams { get { return "DocumentNgrams"; } }
        public static string Translations { get { return "Translations"; } }
    }

    public static class Documents
    {
        public static string Id { get { return "Id"; } }
        public static string PublicId { get { return "PublicId"; } }
        public static string Title { get { return "Title"; } }
        public static string CreationDate { get { return "CreationDate"; } }
        public static string Location { get { return "Location"; } }
        public static string Language { get { return "Language"; } }
    }

    public static class Terms
    {
        public static string Id { get { return "Id"; } }
        public static string Value { get { return "Value"; } }
        public static string NumberOfDocuments { get { return "NumberOfDocuments"; } }
        public static string InverseFrequency { get { return "InverseFrequency"; } }
    }

    public static class Ngrams
    {
        public static string Id { get { return "Id"; } }
        public static string Value { get { return "Value"; } }
    }

    public static class DocumentTerms
    {
        public static string DocumentId { get { return "DocumentId"; } }
        public static string TermId { get { return "TermId"; } }
        public static string NumberOfOccurrences { get { return "NumberOfOccurrences"; } }
        public static string Weight { get { return "Weight"; } }
    }

    public static class DocumentNgrams
    {
        public static string DocumentId { get { return "DocumentId"; } }
        public static string NgramId { get { return "NgramId"; } }
        public static string NumberOfOccurrences { get { return "NumberOfOccurrences"; } }
    }

    public static class Translations
    {
        public static string Id { get { return "Id"; } }
        public static string OriginalValue { get { return "OriginalValue"; } }
        public static string TranslatedValue { get { return "TranslatedValue"; } }
        public static string Probability { get { return "Probability"; } }
    }

    public static class StoredProcedures
    {
        public static string CalculateTermWeightsForDocuments { get { return "sp_calculate_term_weights_for_documents"; } }
    }
}
