﻿using SearchEngine.BLL.Stemmer.Interfaces;
using SearchEngine.Common;
using Snowball;

namespace SearchEngine.BLL.Stemmer
{
    public class StemmerFactory : IStemmerFactory
    {
        private EnglishStemmer _englishStemmer;
        private SpanishStemmer _spanishStemmer;

        public StemmerFactory() { }

        public IStemmer GetStemmer(Language language)
        {
            if (language == Language.English)
            {
                return _englishStemmer = _englishStemmer ?? new EnglishStemmer();
            }

            if (language == Language.Spanish)
            {
                return _spanishStemmer = _spanishStemmer ?? new SpanishStemmer();
            }

            return _englishStemmer = _englishStemmer ?? new EnglishStemmer();
        }
    }
}
