﻿using SearchEngine.Common;

namespace SearchEngine.BLL.Stemmer.Interfaces
{
    public interface IStemmerFactory
    {
        IStemmer GetStemmer(Language language);
    }
}
