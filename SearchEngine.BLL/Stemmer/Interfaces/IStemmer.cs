﻿namespace SearchEngine.BLL.Stemmer.Interfaces
{
    public interface IStemmer
    {
        string Stem(string word);
    }
}
