﻿namespace SearchEngine.BLL.StatisticModels
{
    public class SearchMetricModel
    {
        public int NumberOfRelevantResults { get; set; }
        public int NotIncludedNotRelevantResults { get; set; }

        public SearchMetricModel() { }
    }
}
