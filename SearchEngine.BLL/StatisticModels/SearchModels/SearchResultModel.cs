﻿using SearchEngine.BLL.DTO;

namespace SearchEngine.BLL.StatisticModels
{
    public class SearchResultModel
    {
        public DocumentDTO DocumentDTO { get; set; }
        public string Snippets { get; set; }
        public double DocumentQuerySimilarity { get; set; }
    }
}
