﻿using SearchEngine.BLL.DTO;
using SearchEngine.Common;
using System.Collections.Generic;
using System.Linq;

namespace SearchEngine.BLL.StatisticModels
{
    public class LanguageDefinitionModel
    {
        public DocumentDTO UnrecognizedLanguageDocumentDTO { get; set; }
        public ICollection<DocumentLanguageModel> DocumentLanguageModels { get; set; }
        public Language Language { get; set; }
        // Used for alphabetical language definition.
        public AlphabetLanguageModel AlphabetLanguageModel { get;set; }

        public LanguageDefinitionModel()
        {
            DocumentLanguageModels = new List<DocumentLanguageModel>();
        }

        /* Order ngrams to correct display, like they were build.
           Order document statistic models to display which document was chosen as base when define language. */
        public void OrderNgrams()
        {
            if (UnrecognizedLanguageDocumentDTO != null)
            {
                UnrecognizedLanguageDocumentDTO.NgramDTOs = UnrecognizedLanguageDocumentDTO.NgramDTOs
                    .OrderByDescending(b => b.NumberOfOccurrences)
                    .ThenBy(n => n.Value)
                    .ToList();
            }

            foreach (DocumentLanguageModel documentLanguageModel in DocumentLanguageModels)
            {
                DocumentDTO specifiedLanguageDocumentDTO = documentLanguageModel.SpecifiedLanguageDocumentDTO;

                if (specifiedLanguageDocumentDTO != null)
                {
                    specifiedLanguageDocumentDTO.NgramDTOs = specifiedLanguageDocumentDTO.NgramDTOs
                    .OrderByDescending(b => b.NumberOfOccurrences)
                    .ThenBy(n => n.Value)
                    .ToList();
                }
            }

            DocumentLanguageModels = DocumentLanguageModels.OrderBy(m => m.OutOfPlaceMeasure).ToList();
        }
    }
}
