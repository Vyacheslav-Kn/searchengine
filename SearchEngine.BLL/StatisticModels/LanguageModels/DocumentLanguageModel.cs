﻿using SearchEngine.BLL.DTO;
using System.Collections.Generic;

namespace SearchEngine.BLL.StatisticModels
{
    public class DocumentLanguageModel
    {
        public DocumentDTO SpecifiedLanguageDocumentDTO { get; set; }
        public ICollection<int> NgramDistances { get; set; }
        public int OutOfPlaceMeasure { get; set; }

        public DocumentLanguageModel()
        {
            NgramDistances = new List<int>();
        }
    }
}
