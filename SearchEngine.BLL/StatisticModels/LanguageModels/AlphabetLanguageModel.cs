﻿using SearchEngine.Common;

namespace SearchEngine.BLL.StatisticModels
{
    public class AlphabetLanguageModel
    {
        public int NumberOfSpanCharacterMatches { get; set; }
        public double SpanAlphabetCoefficient { get; set; }

        public int NumberOfEngCharacterMatches { get; set; }
        public double EngAlphabetCoefficient { get; set; }

        public Language Language { get; set; }
    }
}
