﻿using System.Collections.Generic;

namespace SearchEngine.BLL.StatisticModels.TranslateModels
{
    public class ParallelCorporaSentenceModel
    {
        public string Sentence { get; set; }
        public ICollection<WordTranslateModel> WordTranslateModels { get; set; }

        public ParallelCorporaSentenceModel()
        {
            WordTranslateModels = new List<WordTranslateModel>();
        }
    }
}
