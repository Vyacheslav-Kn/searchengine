﻿using SearchEngine.DAL.Entities;
using System.Collections.Generic;

namespace SearchEngine.BLL.StatisticModels.TranslateModels
{
    public class WordTranslateModel
    {
        public string Value { get; set; }
        public ICollection<KeyValuePair<string, double>> TranslationsWithProbability { get; set; }

        public WordTranslateModel()
        {
            TranslationsWithProbability = new List<KeyValuePair<string, double>>();
        }

        public IEnumerable<Translation> ConvertToTranslations()
        {
            List<Translation> translations = new List<Translation>();

            foreach(KeyValuePair<string, double> translationWithProbability in TranslationsWithProbability)
            {
                Translation translation = new Translation
                {
                    OriginalValue = Value,
                    TranslatedValue = translationWithProbability.Key,
                    Probability = translationWithProbability.Value
                };

                translations.Add(translation);
            }

            return translations;
        }
    }
}
