﻿using SearchEngine.BLL.DTO;
using System.Collections.Generic;

namespace SearchEngine.BLL.StatisticModels
{
    public class DocumentStemModel
    {
        public DocumentDTO DocumentDTO { get; set; }
        public ICollection<StemModel> StemModels { get; set; }

        public DocumentStemModel()
        {
            StemModels = new List<StemModel>();
        }
    }
}
