﻿namespace SearchEngine.BLL.StatisticModels
{
    public class StemModel
    {
        public string Term { get; set; }
        public string Stem { get; set; }
        public int NumberOfOccurrence { get; set; }
    }
}
