﻿using SearchEngine.Common;
using System;
using System.Collections.Generic;

namespace SearchEngine.BLL.DTO
{
    public class DocumentDTO : ICloneable
    {
        public string PublicId { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
        public string Location { get; set; }
        public Language Language { get; set; }
        public ICollection<TermDTO> TermDTOs { get; set; }
        public ICollection<NgramDTO> NgramDTOs { get; set; }

        public DocumentDTO()
        {
            TermDTOs = new List<TermDTO>();
            NgramDTOs = new List<NgramDTO>();
        }

        public object Clone()
        {
            DocumentDTO documentDTO = new DocumentDTO
            {
                PublicId = this.PublicId,
                Title = this.Title,
                CreationDate = this.CreationDate,
                Location = this.Location,
                Language = this.Language
            };

            foreach(TermDTO termDTO in this.TermDTOs)
            {
                documentDTO.TermDTOs.Add(new TermDTO {
                    Value = termDTO.Value,
                    NumberOfDocuments = termDTO.NumberOfDocuments,
                    InverseFrequency = termDTO.InverseFrequency,
                    NumberOfOccurrences = termDTO.NumberOfOccurrences,
                    Weight = termDTO.Weight
                });
            }

            foreach(NgramDTO ngramDTO in this.NgramDTOs)
            {
                documentDTO.NgramDTOs.Add(new NgramDTO {
                    Value = ngramDTO.Value,
                    NumberOfOccurrences = ngramDTO.NumberOfOccurrences
                });
            }

            return documentDTO;
        }
    }
}
