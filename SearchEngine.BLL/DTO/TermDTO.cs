﻿using System.Collections.Generic;

namespace SearchEngine.BLL.DTO
{
    public class TermDTO
    {
        public string Value { get; set; }
        public int NumberOfDocuments { get; set; }
        public double InverseFrequency { get; set; }
        public ICollection<DocumentDTO> Documents { get; set; }

        // This fields are used when slicing personal document info from database
        public int NumberOfOccurrences { get; set; }
        public double Weight { get; set; }

        public TermDTO()
        {
            Documents = new List<DocumentDTO>();
        }
    }
}
