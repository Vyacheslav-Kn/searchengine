﻿namespace SearchEngine.BLL.DTO
{
    public class NgramDTO
    {
        public string Value { get; set; }

        // This fields are used when slicing personal document info from database
        public int NumberOfOccurrences { get; set; }
    }
}
