﻿using AutoMapper;
using SearchEngine.BLL.DTO;
using SearchEngine.DAL.Entities;

namespace SearchEngine.BLL.Mapper
{
    public class TermAndTermDTO : Profile
    {
        public TermAndTermDTO()
        {
            AllowNullCollections = true;

            CreateMap<Term, TermDTO>();
            CreateMap<TermDTO, Term>();
        }
    }
}
