﻿using AutoMapper;
using SearchEngine.BLL.DTO;
using SearchEngine.DAL.Entities;

namespace SearchEngine.BLL.Mapper
{
    public class DocumentAndDocumentDTO : Profile
    {
        public DocumentAndDocumentDTO()
        {
            AllowNullCollections = true;

            CreateMap<Document, DocumentDTO>()
                .ForMember(dt => dt.TermDTOs, opt => opt.MapFrom(d => d.Terms))
                .ForMember(dt => dt.NgramDTOs, opt => opt.MapFrom(d => d.Ngrams));

            CreateMap<DocumentDTO, Document>()
                .ForMember(dt => dt.Terms, opt => opt.MapFrom(d => d.TermDTOs))
                .ForMember(dt => dt.Ngrams, opt => opt.MapFrom(d => d.NgramDTOs));
        }
    }
}
