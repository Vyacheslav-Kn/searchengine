﻿using AutoMapper;
using SearchEngine.BLL.DTO;
using SearchEngine.DAL.Entities;

namespace SearchEngine.BLL.Mapper
{
    public class NgramAndNgramDTO : Profile
    {
        public NgramAndNgramDTO()
        {
            AllowNullCollections = true;

            CreateMap<Ngram, NgramDTO>();
            CreateMap<NgramDTO, Ngram>();
        }
    }
}
