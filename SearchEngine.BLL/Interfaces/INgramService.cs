﻿using SearchEngine.BLL.DTO;
using SearchEngine.BLL.StatisticModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Interfaces
{
    public interface INgramService
    {
        Task CreateDocumentNgramsAsync(IEnumerable<NgramDTO> ngramDTOs, int documentId);
        IEnumerable<NgramDTO> GetNgrams(IEnumerable<string> termOccurrences);
        IEnumerable<NgramDTO> BuildNgrams(IEnumerable<string> termOccurrences);
        IEnumerable<NgramDTO> GetMostPopularNgrams(IEnumerable<NgramDTO> ngramDTOs);
        Task<LanguageDefinitionModel> DefineLanguageWithStatisticsAsync(DocumentDTO unrecognizedLanguageDocumentDTO);

        DocumentLanguageModel GetDocumentLanguageModel(DocumentDTO specifiedLanguageDocumentDTO,
             DocumentDTO testDocumentDTO);
    }
}
