﻿using SearchEngine.BLL.StatisticModels;

namespace SearchEngine.BLL.Interfaces
{
    public interface IAlphabetService
    {
        AlphabetLanguageModel DefineLanguageWithStatistics(string content);
    }
}
