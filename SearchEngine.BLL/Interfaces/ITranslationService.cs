﻿using SearchEngine.BLL.StatisticModels.TranslateModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Interfaces
{
    public interface ITranslationService
    {
        ICollection<ParallelCorporaSentenceModel> BuildParallelCorporaSentenceModels(string content);
        ICollection<WordTranslateModel> GetTranslateModels(ICollection<ParallelCorporaSentenceModel> sentenceModels);
        Task CreateManyAsync(ICollection<WordTranslateModel> translateModels);
        string PrepareContentForTranslation(string content);
        Task<ICollection<WordTranslateModel>> Translate(string content);
    }
}
