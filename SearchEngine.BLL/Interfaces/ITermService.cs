﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Interfaces
{
    public interface ITermService
    {
        IEnumerable<KeyValuePair<string, int>> GetTermsWithOccurrences(string content, bool removeStopwords = true);
        Task CreateDocumentTermsAsync(IEnumerable<KeyValuePair<string, int>> termsWithOccurrences, int documentId);
        Task CalculateWeightsAsync();
    }
}
