﻿using SearchEngine.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Interfaces
{
    public interface IDocumentService
    {
        Task<DocumentDTO> GetByPublicIdAsync(string publicId);
        Task<DocumentDTO> GetByPublicIdWithNgramsAsync(string publicId);
        Task<int> CreateAsync(DocumentDTO documentDTO);
        Task<IEnumerable<DocumentDTO>> GetDocumentsWithQueryWord(IEnumerable<string> queryWords);
        Task<int> Count();
    }
}
