﻿using System;

namespace SearchEngine.BLL.Interfaces
{
    public interface IServiceFactory : IDisposable
    {
        IDocumentService DocumentService { get; }
        ITermService TermService { get; }
        INgramService NgramService { get; }
        IAlphabetService AlphabetService { get; }
        ITranslationService TranslationService { get; }

        void CommitChanges();
        void RollbackChanges();
    }
}
