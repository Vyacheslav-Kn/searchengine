﻿using AutoMapper;
using SearchEngine.BLL.DTO;
using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.StatisticModels;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Services
{
    public class NgramService : INgramService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public NgramService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateDocumentNgramsAsync(IEnumerable<NgramDTO> ngramDTOs, int documentId)
        {
            IEnumerable<Ngram> ngrams = _mapper.Map<IEnumerable<NgramDTO>, IEnumerable<Ngram>>(ngramDTOs);

            await _unitOfWork.NgramRepository.CreateManyAsync(ngrams, documentId);
        }

        public IEnumerable<NgramDTO> GetNgrams(IEnumerable<string> termOccurrences)
        {
            IEnumerable<NgramDTO> ngramDTOs = BuildNgrams(termOccurrences);

            IEnumerable<NgramDTO> mostPopularNgrams = GetMostPopularNgrams(ngramDTOs);

            return mostPopularNgrams;
        }

        public IEnumerable<NgramDTO> BuildNgrams(IEnumerable<string> terms)
        {
            List<NgramDTO> ngramDTOs = new List<NgramDTO>();

            int ngramLength = 3;

            foreach(string term in terms)
            {
                bool ngramExists = term.Length >= ngramLength;

                if (ngramExists)
                {
                    List<char> listOfCharOccurrences = term.ToList();

                    int singleOccurrence = 1;

                    for (int index = 0; index < listOfCharOccurrences.Count() - ngramLength + 1; index++)
                    {
                        string value = string.Join("", listOfCharOccurrences.GetRange(index, ngramLength)).ToLower();
                        NgramDTO checkedNgramDTO = ngramDTOs.FirstOrDefault(b => b.Value == value);

                        if (checkedNgramDTO != null)
                        {
                            checkedNgramDTO.NumberOfOccurrences += singleOccurrence;
                        }
                        else
                        {
                            ngramDTOs.Add(new NgramDTO
                            {
                                Value = value,
                                NumberOfOccurrences = singleOccurrence
                            });
                        }
                    }
                }
            }

            return ngramDTOs;
        }

        public IEnumerable<NgramDTO> GetMostPopularNgrams(IEnumerable<NgramDTO> ngramDTOs)
        {
            int maxNumberOfNgrams = 300;

            return ngramDTOs.OrderByDescending(b => b.NumberOfOccurrences).Take(maxNumberOfNgrams);
        }

        public async Task<LanguageDefinitionModel> DefineLanguageWithStatisticsAsync(DocumentDTO unrecognizedLanguageDocumentDTO)
        {
            IEnumerable<Document> documents = await _unitOfWork.DocumentRepository.GetDocumentsWithNgrams();
            IEnumerable<DocumentDTO> documentDTOs = _mapper.Map<IEnumerable<Document>, IEnumerable<DocumentDTO>>(documents);

            LanguageDefinitionModel languageDefinitionModel = new LanguageDefinitionModel
            {
                UnrecognizedLanguageDocumentDTO = unrecognizedLanguageDocumentDTO
            };

            foreach(DocumentDTO documentDTO in documentDTOs)
            {
                DocumentLanguageModel documentLanguageModel = GetDocumentLanguageModel(documentDTO, unrecognizedLanguageDocumentDTO);

                languageDefinitionModel.DocumentLanguageModels.Add(documentLanguageModel);
            }

            languageDefinitionModel.Language = languageDefinitionModel.DocumentLanguageModels
                .OrderBy(d => d.OutOfPlaceMeasure)
                .First()
                .SpecifiedLanguageDocumentDTO.Language;

            return languageDefinitionModel;
        }

        public DocumentLanguageModel GetDocumentLanguageModel(DocumentDTO specifiedLanguageDocumentDTO, DocumentDTO testDocumentDTO)
        {
            DocumentLanguageModel documentLanguageModel = new DocumentLanguageModel
            {
                SpecifiedLanguageDocumentDTO = specifiedLanguageDocumentDTO
            };

            NgramDTO[] specifiedLanguageNgramDTOs = specifiedLanguageDocumentDTO.NgramDTOs
                .OrderByDescending(n => n.NumberOfOccurrences)
                .ThenBy(n => n.Value)
                .ToArray();

            NgramDTO[] testNgramDTOs = testDocumentDTO.NgramDTOs
                .OrderByDescending(n => n.NumberOfOccurrences)
                .ThenBy(n => n.Value)
                .ToArray();

            int maxNgramDistance = 300;
            int minNgramDistance = 0;

            for(int index = 0; index < testNgramDTOs.Length; index++)
            {
                string ngramValue = testNgramDTOs[index].Value;

                bool ngramExistsInSpecifiedLanguageDocumentDTO = specifiedLanguageNgramDTOs.Any(b => b.Value == ngramValue);

                if (ngramExistsInSpecifiedLanguageDocumentDTO)
                {
                    bool ngramHasSamePosition = index < specifiedLanguageNgramDTOs.Length
                        ? ngramValue == specifiedLanguageNgramDTOs[index].Value
                        : false;

                    if (ngramHasSamePosition)
                    {
                        documentLanguageModel.NgramDistances.Add(minNgramDistance);
                    }
                    else
                    {
                        int positionInSpecifiedLanguageNgramDTOs = Array
                            .FindIndex(specifiedLanguageNgramDTOs, b => b.Value == ngramValue);

                        int positionDifference = Math.Abs(index - positionInSpecifiedLanguageNgramDTOs);

                        documentLanguageModel.NgramDistances.Add(positionDifference);
                    }
                }
                else
                {
                    documentLanguageModel.NgramDistances.Add(maxNgramDistance);
                }
            }

            documentLanguageModel.OutOfPlaceMeasure = documentLanguageModel.NgramDistances.Sum();

            return documentLanguageModel;
        }
    }
}
