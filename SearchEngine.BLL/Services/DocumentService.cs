﻿using AutoMapper;
using SearchEngine.BLL.DTO;
using SearchEngine.BLL.Interfaces;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Services
{
    public class DocumentService : IDocumentService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public DocumentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<DocumentDTO> GetByPublicIdAsync(string publicId)
        {
            Document document = await _unitOfWork.DocumentRepository.GetByPublicIdAsync(publicId);

            DocumentDTO documentDTO = document != null ? _mapper.Map<Document, DocumentDTO>(document) : null;

            return documentDTO;
        }

        public async Task<DocumentDTO> GetByPublicIdWithNgramsAsync(string publicId)
        {
            Document document = await _unitOfWork.DocumentRepository.GetByPublicIdWithNgramsAsync(publicId);

            DocumentDTO documentDTO = document != null ? _mapper.Map<Document, DocumentDTO>(document) : null;

            return documentDTO;
        }

        public async Task<int> CreateAsync(DocumentDTO documentDTO)
        {
            Document document = _mapper.Map<Document>(documentDTO);
            document.CreationDate = DateTime.Now;

            int documentId = await _unitOfWork.DocumentRepository.CreateAsync(document);

            return documentId;
        }

        public async Task<IEnumerable<DocumentDTO>> GetDocumentsWithQueryWord(IEnumerable<string> queryWords)
        {
            IEnumerable<Document> documents = await _unitOfWork.DocumentRepository.GetDocumentsWithQueryWord(queryWords);

            return _mapper.Map<IEnumerable<Document>, IEnumerable<DocumentDTO>>(documents);
        }

        public async Task<int> Count()
        {
            int numberOfSavedDocuments = await _unitOfWork.DocumentRepository.Count();

            return numberOfSavedDocuments;
        }
    }
}
