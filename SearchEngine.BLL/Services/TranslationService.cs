﻿using AutoMapper;
using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.StatisticModels.TranslateModels;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SearchEngine.BLL.Services
{
    public class TranslationService : ITranslationService
    {
        private IUnitOfWork _unitOfWork;

        private int MinWordLength = 2;

        public TranslationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ICollection<ParallelCorporaSentenceModel> BuildParallelCorporaSentenceModels(string content)
        {
            ICollection<ParallelCorporaSentenceModel> sentenceModels = new List<ParallelCorporaSentenceModel>();

            ICollection<Tuple<string, string>> parallelSentences = GetParallelSentences(content);

            sentenceModels = GetParallelCorporaSentenceModels(parallelSentences);

            return sentenceModels;
        }

        private ICollection<Tuple<string, string>> GetParallelSentences(string content)
        {
            ICollection<Tuple<string, string>> parallelSentences = new List<Tuple<string, string>>();

            string[] corporaSentences = content.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            int numberOfSentencesInCorporaLine = 2;

            foreach(string corporaSentence in corporaSentences)
            {
                string[] sentences = corporaSentence
                    .Split(".", StringSplitOptions.None)
                    .Take(numberOfSentencesInCorporaLine)
                    .ToArray();

                if (sentences.Length == numberOfSentencesInCorporaLine)
                {
                    string clearedSentenceOriginal = PrepareContentForTranslation(sentences[0]);

                    string clearedSentenceTranslated = PrepareContentForTranslation(sentences[1]);

                    parallelSentences.Add(new Tuple<string, string>(clearedSentenceOriginal, clearedSentenceTranslated));
                }
            }

            return parallelSentences;
        }

        public string PrepareContentForTranslation(string content)
        {
            string clearedContent = new string(content
                        .Where(x => char.IsWhiteSpace(x) || char.IsLetterOrDigit(x))
                        .ToArray());
            clearedContent = Regex.Replace(clearedContent, @"\s+", " ");

            return clearedContent;
        }

        private ICollection<ParallelCorporaSentenceModel> GetParallelCorporaSentenceModels(
            ICollection<Tuple<string, string>> parallelSentences)
        {
            ICollection<ParallelCorporaSentenceModel> sentenceModels = new List<ParallelCorporaSentenceModel>();

            foreach(Tuple<string, string> tupleOfSentences in parallelSentences)
            {
                ParallelCorporaSentenceModel sentenceModel = new ParallelCorporaSentenceModel
                {
                    Sentence = tupleOfSentences.Item1
                };

                string originalSentence = tupleOfSentences.Item1;
                string translatedSentence = tupleOfSentences.Item2;

                string[] originalSentenceWords = originalSentence.Split(" ", StringSplitOptions.None);
                string[] translatedSentenceWords = translatedSentence.Split(" ", StringSplitOptions.None);

                double translationProbabilityForSentenceWords = 1 / (double)translatedSentenceWords.Count() * 100;
                ICollection<KeyValuePair<string, double>> translationsWithProbability = new List<KeyValuePair<string, double>>();
                foreach(string translatedSentenceWord in translatedSentenceWords)
                {
                    translationsWithProbability.Add(new KeyValuePair<string, double>(
                        translatedSentenceWord, translationProbabilityForSentenceWords));
                }

                foreach(string originalSentenceWord in originalSentenceWords)
                {
                    bool translationWordsFound = sentenceModel.WordTranslateModels.Any(m => m.Value == originalSentenceWord);

                    if (!translationWordsFound)
                    {
                        sentenceModel.WordTranslateModels.Add(new WordTranslateModel
                        {
                            Value = originalSentenceWord,
                            TranslationsWithProbability = translationsWithProbability
                        });
                    }
                }

                sentenceModels.Add(sentenceModel);
            }

            return sentenceModels;
        }

        public ICollection<WordTranslateModel> GetTranslateModels(
            ICollection<ParallelCorporaSentenceModel> sentenceModels)
        {
            ICollection<WordTranslateModel> dictionaryTranslateModels = new List<WordTranslateModel>();

            foreach (ParallelCorporaSentenceModel sentenceModel in sentenceModels)
            {
                foreach(WordTranslateModel wordTranslateModel in sentenceModel.WordTranslateModels)
                {
                    if (wordTranslateModel.Value.Length > MinWordLength)
                    {
                        bool isInDictionary = dictionaryTranslateModels.Any(m => m.Value == wordTranslateModel.Value);

                        if (!isInDictionary)
                        {
                            ICollection<WordTranslateModel> translateModelsForCurrentWord = new List<WordTranslateModel>();

                            translateModelsForCurrentWord = sentenceModels
                                .Where(m => m.WordTranslateModels
                                    .Any(tm => tm.Value == wordTranslateModel.Value))
                                .Select(m => m.WordTranslateModels
                                    .Where(tm => tm.Value == wordTranslateModel.Value)
                                    .First())
                                .ToList();

                            WordTranslateModel dictionaryTranslateModel = GetDictionaryTranslateModel(translateModelsForCurrentWord);

                            dictionaryTranslateModels.Add(dictionaryTranslateModel);
                        }
                    }
                }
            }

            return dictionaryTranslateModels;
        }

        private WordTranslateModel GetDictionaryTranslateModel(ICollection<WordTranslateModel> translateModelsForWord)
        {
            WordTranslateModel wordTranslateModel = null;
            int maxNumberOfPossibleTranslations = 3;

            Dictionary<string, int> translationsWithOccurrences = new Dictionary<string, int>();
            int singleTranslationOccurrence = 1;

            foreach(WordTranslateModel translateModel in translateModelsForWord)
            {
                foreach(KeyValuePair<string, double> possibleTranslation in translateModel.TranslationsWithProbability)
                {
                    if (!translationsWithOccurrences.ContainsKey(possibleTranslation.Key))
                    {
                        translationsWithOccurrences.Add(possibleTranslation.Key, singleTranslationOccurrence);
                    }
                    else
                    {
                        translationsWithOccurrences[possibleTranslation.Key] += singleTranslationOccurrence;
                    }
                }
            }

            int numberOfAllPossibleTranslations = translationsWithOccurrences.Count();
            ICollection<KeyValuePair<string, double>> dictionaryTranslationsWithProbability = translationsWithOccurrences
                .Where(tr => tr.Key.Length > MinWordLength)
                .OrderByDescending(tr => tr.Value)
                .Take(maxNumberOfPossibleTranslations)
                .Select(tr => {
                    double translationProbability = tr.Value / (double)numberOfAllPossibleTranslations * 100;

                    return new KeyValuePair<string, double>(tr.Key, translationProbability);
                })
                .ToList();

            wordTranslateModel = new WordTranslateModel
            {
                Value = translateModelsForWord.First().Value,
                TranslationsWithProbability = dictionaryTranslationsWithProbability
            };

            return wordTranslateModel;
        }

        public async Task CreateManyAsync(ICollection<WordTranslateModel> translateModels)
        {
            List<Translation> translations = new List<Translation>();

            foreach(WordTranslateModel wordTranslateModel in translateModels)
            {
                IEnumerable<Translation> wordTranslations = wordTranslateModel.ConvertToTranslations();

                translations.AddRange(wordTranslations);
            }

            await _unitOfWork.TranslationRepository.CreateManyAsync(translations);
        }

        public async Task<ICollection<WordTranslateModel>> Translate(string content)
        {
            ICollection<WordTranslateModel> wordTranslateModels = new List<WordTranslateModel>();

            string[] words = content.Split(" ", StringSplitOptions.None);

            foreach (string word in words)
            {
                if(word.Length > MinWordLength)
                {
                    IEnumerable<Translation> translations = await _unitOfWork.TranslationRepository
                        .GetTranslationsAsync(word);

                    if (translations.Any())
                    {
                        WordTranslateModel wordTranslateModel = new WordTranslateModel
                        {
                            Value = word
                        };

                        wordTranslateModel.TranslationsWithProbability = translations
                            .Select(tr => new KeyValuePair<string, double>(tr.TranslatedValue, tr.Probability))
                            .ToList();

                        wordTranslateModels.Add(wordTranslateModel);
                    }
                }
            }

            return wordTranslateModels;
        }
    }
}
