﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SearchEngine.BLL.Interfaces;
using SearchEngine.DAL.Interfaces;

namespace SearchEngine.BLL.Services
{
    public class ServiceFactory : IServiceFactory
    {
        private IConfiguration _configuration;
        private IHostingEnvironment _hostingEnvironment;
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        private IDocumentService _documentService;
        private ITermService _termService;
        private INgramService _ngramService;
        private IAlphabetService _alphabetService;
        private ITranslationService _translationService;

        public ServiceFactory(IUnitOfWork unitOfWork, IMapper mapper, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IDocumentService DocumentService
        {
            get
            {
                return _documentService = _documentService ?? new DocumentService(_unitOfWork, _mapper);
            }
        }

        public ITermService TermService
        {
            get
            {
                return _termService = _termService ?? new TermService(_unitOfWork, _hostingEnvironment, _configuration);
            }
        }

        public INgramService NgramService
        {
            get
            {
                return _ngramService = _ngramService ?? new NgramService(_unitOfWork, _mapper);
            }
        }

        public IAlphabetService AlphabetService
        {
            get
            {
                return _alphabetService = _alphabetService ?? new AlphabetService();
            }
        }

        public ITranslationService TranslationService
        {
            get
            {
                return _translationService = _translationService ?? new TranslationService(_unitOfWork);
            }
        }

        public void CommitChanges()
        {
            _unitOfWork.Commit();
        }

        public void RollbackChanges()
        {
            _unitOfWork.Rollback();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
