﻿using SearchEngine.BLL.Interfaces;
using SearchEngine.BLL.StatisticModels;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SearchEngine.BLL.Services
{
    public class AlphabetService : IAlphabetService
    {
        public AlphabetService() { }

        public AlphabetLanguageModel DefineLanguageWithStatistics(string content)
        {
            string contentLower= content.ToLower();

            Regex spanCharacterRegex = new Regex("\\p{L}");
            int spanAlphabetPower = 31; // Real number 27, but users upload text with mixed symbols.

            int numberOfSpanCharacterMatches = CountCharacterMatches(spanCharacterRegex, contentLower);
            double spanAlphabetCoefficient = CountAlphabetCoefficient(numberOfSpanCharacterMatches, spanAlphabetPower);

            Regex engCharacterRegex = new Regex("\\w");
            int engAlphabetPower = 26;

            int numberOfEngCharacterMatches = CountCharacterMatches(engCharacterRegex, contentLower);
            double engAlphabetCoefficient = CountAlphabetCoefficient(numberOfEngCharacterMatches, engAlphabetPower);

            AlphabetLanguageModel alphabetLanguageModel = new AlphabetLanguageModel
            {
                NumberOfSpanCharacterMatches = numberOfSpanCharacterMatches,
                SpanAlphabetCoefficient = spanAlphabetCoefficient,
                NumberOfEngCharacterMatches = numberOfEngCharacterMatches,
                EngAlphabetCoefficient = engAlphabetCoefficient,
                Language = spanAlphabetCoefficient > engAlphabetCoefficient
                    ? Common.Language.Spanish
                    : Common.Language.English
            };

            return alphabetLanguageModel;
        }

        private int CountCharacterMatches(Regex characterRegex, string content)
        {
            Dictionary<string, int> charactersWithOccurrences = new Dictionary<string, int>();
            int singleOccurrence = 1;

            foreach(Match match in characterRegex.Matches(content))
            {
                if (charactersWithOccurrences.ContainsKey(match.Value))
                {
                    charactersWithOccurrences[match.Value] += singleOccurrence;
                }
                else
                {
                    charactersWithOccurrences.Add(match.Value, singleOccurrence);
                }
            }

            return charactersWithOccurrences.Count;
        }

        private double CountAlphabetCoefficient(int numberOfCharacterMatches, int alphabetPower)
        {
            return numberOfCharacterMatches / (double)alphabetPower;
        }
    }
}
