﻿using SearchEngine.BLL.Interfaces;
using SearchEngine.DAL.Entities;
using SearchEngine.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Newtonsoft.Json;

namespace SearchEngine.BLL.Services
{
    public class TermService : ITermService
    {
        private IConfiguration _configuration;
        private IHostingEnvironment _hostingEnvironment;
        private IUnitOfWork _unitOfWork;

        public TermService(IUnitOfWork unitOfWork, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<KeyValuePair<string, int>> GetTermsWithOccurrences(string text, bool removeStopwords = true)
        {
            ICollection<string> termOccurrences = FindTermOccurrences(text, removeStopwords);

            IEnumerable<KeyValuePair<string, int>> termsWithOccurrences = CountTermOccurrences(termOccurrences);

            return termsWithOccurrences.OrderBy(x => x.Value);
        }

        public async Task CreateDocumentTermsAsync(IEnumerable<KeyValuePair<string, int>> termsWithOccurrences, int documentId)
        {
            IEnumerable<Term> savedTerms = await _unitOfWork.TermRepository.GetSavedTermsAsync(termsWithOccurrences.Select(x => x.Key));

            IEnumerable<KeyValuePair<string, int>> termsWithOccurrencesToCreate = termsWithOccurrences
                .Where(x => !savedTerms.Any(s => s.Value == x.Key));

            if (termsWithOccurrencesToCreate.Count() > 0)
            {
                await _unitOfWork.TermRepository.CreateManyAsync(termsWithOccurrencesToCreate, documentId);
            }

            if (savedTerms.Count() > 0)
            {
                IDictionary<int, int> savedTermIdsWithOccurrences = new Dictionary<int, int>();

                foreach(Term term in savedTerms)
                {
                    int numberOfTermOccurrences = termsWithOccurrences
                        .Where(x => x.Key == term.Value)
                        .Select(t => t.Value)
                        .First();

                    savedTermIdsWithOccurrences.Add(term.Id, numberOfTermOccurrences);
                }

                await _unitOfWork.TermRepository.UpdateManyAsync(savedTermIdsWithOccurrences, documentId);
            }
        }

        public async Task CalculateWeightsAsync()
        {
            await _unitOfWork.TermRepository.CalculateWeightsAsync();
        }

        private ICollection<string> FindTermOccurrences(string text, bool removeStopwords = true)
        {
            string termsPattern = "[\\p{L}]+[^\\s]?[\\p{L}]+|[\\p{L}]";
            Regex regex = new Regex(termsPattern);

            ICollection<string> termOccurrences = regex.Matches(text).Select(m => m.Value).ToList();

            if (!removeStopwords)
            {
                return termOccurrences;
            }

            ICollection<string> termOccurrencesWithoutStopwords = RemoveStopwordOccurrences(termOccurrences);

            return termOccurrencesWithoutStopwords;
        }

        private ICollection<string> RemoveStopwordOccurrences(ICollection<string> termOccurrences)
        {
            string stopwordsFolderPath = $"{_hostingEnvironment.WebRootPath}\\{_configuration["StopwordFolderName"]}";

            List<string> stopwords = new List<string>();

            foreach(string filePath in Directory.GetFiles(stopwordsFolderPath, "*.json"))
            {
                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    string fileContent = streamReader.ReadToEnd();
                    IEnumerable<string> fileStopwords = JsonConvert.DeserializeObject<IEnumerable<string>>(fileContent);

                    stopwords.AddRange(fileStopwords);
                }
            }

            ICollection<string> termOccurrencesWithoutStopwords = termOccurrences.Where(x => !stopwords.Any(s => s == x.ToLower())).ToList();

            return termOccurrencesWithoutStopwords;
        }

        private IEnumerable<KeyValuePair<string, int>> CountTermOccurrences(ICollection<string> termOccurrences)
        {
            IDictionary<string, int> termsWithOccurrences = new Dictionary<string, int>();

            int singleTermOccurrence = 1;
            StringBuilder termBuilder = new StringBuilder();

            foreach (string term in termOccurrences)
            {
                termBuilder.Append(term);

                if (termsWithOccurrences.ContainsKey(termBuilder.ToString()))
                {
                    termsWithOccurrences[termBuilder.ToString()] += singleTermOccurrence;
                }
                else
                {
                    termsWithOccurrences.Add(termBuilder.ToString(), singleTermOccurrence);
                }

                termBuilder.Clear();
            }

            return termsWithOccurrences;
        }
    }
}
