﻿CREATE TABLE [dbo].[DocumentTerms] (
    [DocumentId]          INT        NOT NULL,
    [TermId]              INT        NOT NULL,
    [NumberOfOccurrences] INT        NULL,
    [Weight]              FLOAT (53) NULL,
    CONSTRAINT [PK_Document_Term] PRIMARY KEY CLUSTERED ([DocumentId] ASC, [TermId] ASC),
    FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[Documents] ([Id]),
    FOREIGN KEY ([TermId]) REFERENCES [dbo].[Terms] ([Id])
);

