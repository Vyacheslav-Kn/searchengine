﻿CREATE TABLE [dbo].[Translations] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [OriginalValue]   NVARCHAR (100) NOT NULL,
    [TranslatedValue] NVARCHAR (100) NOT NULL,
    [Probability]     FLOAT (53)     NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_value]
    ON [dbo].[Translations]([OriginalValue] ASC);

