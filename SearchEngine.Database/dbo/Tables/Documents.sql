﻿CREATE TABLE [dbo].[Documents] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [PublicId]     NVARCHAR (250) NOT NULL,
    [Title]        NVARCHAR (200) NOT NULL,
    [CreationDate] DATETIME       NOT NULL,
    [Location]     NVARCHAR (500) NOT NULL,
    [Language]     INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_title]
    ON [dbo].[Documents]([Title] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_publicId]
    ON [dbo].[Documents]([PublicId] ASC);

