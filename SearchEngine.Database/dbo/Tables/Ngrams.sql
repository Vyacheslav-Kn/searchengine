﻿CREATE TABLE [dbo].[Ngrams] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Value]             NVARCHAR (300) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_value]
    ON [dbo].[Ngrams]([Value] ASC);

