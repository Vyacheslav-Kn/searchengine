﻿CREATE TABLE [dbo].[DocumentNgrams] (
    [DocumentId]          INT        NOT NULL,
    [NgramId]              INT        NOT NULL,
    [NumberOfOccurrences] INT        NOT NULL,
    CONSTRAINT [PK_Document_Ngram] PRIMARY KEY CLUSTERED ([DocumentId] ASC, [NgramId] ASC),
    FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[Documents] ([Id]),
    FOREIGN KEY ([NgramId]) REFERENCES [dbo].[Ngrams] ([Id])
);
