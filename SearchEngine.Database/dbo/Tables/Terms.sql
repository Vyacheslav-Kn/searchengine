﻿CREATE TABLE [dbo].[Terms] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Value]             NVARCHAR (100) NOT NULL,
    [NumberOfDocuments] INT            NOT NULL,
    [InverseFrequency]  FLOAT (53)     NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_value]
    ON [dbo].[Terms]([Value] ASC);

