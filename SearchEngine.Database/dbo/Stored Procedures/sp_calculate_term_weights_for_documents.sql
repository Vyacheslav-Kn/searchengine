﻿
CREATE PROCEDURE sp_calculate_term_weights_for_documents AS
BEGIN
	DECLARE @NumberOfDocuments FLOAT;
	SET @NumberOfDocuments = (SELECT COUNT(*) FROM Documents);

	UPDATE Terms 
	SET InverseFrequency = LOG(ISNULL((@NumberOfDocuments / NULLIF((SELECT COUNT(*) FROM DocumentTerms WHERE TermId = Id), 0)), @NumberOfDocuments));

	UPDATE DocumentTerms
	SET Weight = NumberOfOccurrences * (SELECT InverseFrequency FROM Terms WHERE Id = TermId);
END;
